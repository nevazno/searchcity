package com.searchcity.mvp.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.searchcity.R;
import com.searchcity.model.CityItem;
import com.searchcity.mvp.PositiveButtonCallback;
import com.searchcity.mvp.holder.CityFragmentHolder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CityFragmentView {

    private CityFragmentHolder holder;
    private Activity activity;
    private PositiveButtonCallback callback;

    public CityFragmentView(CityFragmentHolder holder, Activity activity) {
        this.holder = holder;
        this.activity = activity;
    }

    public void showDeleteCityDialog(){
        if(activity != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
            builder.setTitle(R.string.alert);
            builder.setMessage(R.string.alert_text);
            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(callback != null){
                        callback.click("");
                    }
                    if(activity != null){
                        activity.onBackPressed();
                    }
                }
            });
            builder.setNegativeButton(R.string.cencel, null);
            builder.show();
        }
    }

    public void showResponse(final CityItem item){
        holder.temp.setText(String.valueOf(item.getTemperature().getMetric().getValue()));
        holder.weatherText.setText(item.getWeatherText());
        Date date = new Date(Long.parseLong(String.valueOf(item.getEpochTime()).concat("000")));
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy HH:mm", Locale.getDefault());
        holder.epochTime.setText(df2.format(date));
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getMobileLink()));
                activity.startActivity(browserIntent);
            }
        });
        Glide.with(activity)
                .load("https://apidev.accuweather.com/developers/Media/Default/WeatherIcons/0"+item.getWeatherIcon()+"-s.png")
                .placeholder(R.drawable.sun)
                .error(R.drawable.sun)
                .centerCrop()
                .into(holder.img);
    }

    public void setCallback(PositiveButtonCallback callback) {
        this.callback = callback;
    }
}
