package com.searchcity.mvp.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;

import com.searchcity.MainActivity;
import com.searchcity.R;
import com.searchcity.adapter.RecycleAdapter;
import com.searchcity.holder.CityHolder;
import com.searchcity.model.City;
import com.searchcity.mvp.PositiveButtonCallback;
import com.searchcity.mvp.holder.MainFragmentHolder;
import com.searchcity.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MainFragmentView {

    private MainFragmentHolder holder;
    private Activity activity;
    private PositiveButtonCallback callback;

    public MainFragmentView(MainFragmentHolder holder, Activity activity) {
        this.holder = holder;
        this.activity = activity;
        RecycleAdapter adapter = getCityAdapter(new ArrayList<City>());
        this.holder.recyclerView.setAdapter(adapter);
    }

    private RecycleAdapter getCityAdapter(List<City> cities) {
        return new RecycleAdapter<City, CityHolder>(cities) {
            @Override
            public CityHolder setViewHolder(ViewGroup parent) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_city, parent, false);
                return new CityHolder(view);
            }

            @Override
            public void onBindData(CityHolder holder, final City city, int position) {
                holder.name.setText(city.getLocalizedName());
                holder.country.setText(city.getCountry().getLocalizedName());
                holder.area.setText(city.getAdministrativeArea().getLocalizedName());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(activity != null){
                            ((MainActivity)activity).showCityFragment(city);
                        }
                    }
                });
            }
        };
    }

    public void showListCities(List<City> cities){
        holder.recyclerView.setAdapter(getCityAdapter(cities));
    }

    public void showAddCityDialog(){
        final EditText input = new EditText(activity);
        input.setTextColor(Color.WHITE);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        if(activity != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
            builder.setTitle(R.string.search);
            builder.setMessage(R.string.edit_city_name);
            builder.setPositiveButton(R.string.search, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final String value = input.getText().toString();
                    if (value.length() > 0 && callback != null) {
                        callback.click(value);
                    }
                }
            });
            builder.setNegativeButton(R.string.cencel, null);
            builder.setView(input);
            builder.show();
        }
    }

    public void showProgress(){
        holder.progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress(){
        holder.progressBar.setVisibility(View.GONE);
    }

    public void showToast(String message){
        Utils.showToast(activity, message);
    }

    public void setCallback(PositiveButtonCallback callback) {
        this.callback = callback;
    }
}
