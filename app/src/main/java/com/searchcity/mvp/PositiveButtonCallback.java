package com.searchcity.mvp;

public interface PositiveButtonCallback {
    void click(String value);
}
