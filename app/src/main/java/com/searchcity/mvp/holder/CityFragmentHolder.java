package com.searchcity.mvp.holder;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.searchcity.R;

public class CityFragmentHolder {

    public TextView temp;
    public TextView weatherText;
    public TextView epochTime;
    public ImageView img;
    public Button btn;

    public CityFragmentHolder(View parentView) {
        CoordinatorLayout rl = parentView.findViewById(R.id.cl);
        rl.setBackgroundResource(R.drawable.fon);
        temp = parentView.findViewById(R.id.tv_temp);
        weatherText = parentView.findViewById(R.id.weather_text);
        epochTime = parentView.findViewById(R.id.epoch_time);
        img = parentView.findViewById(R.id.img);
        btn = parentView.findViewById(R.id.btn);
    }
}
