package com.searchcity.mvp.holder;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.searchcity.R;

public class MainFragmentHolder {
    public final RecyclerView recyclerView;
    public final ProgressBar progressBar;

    public MainFragmentHolder(View parentView, Context context) {
        recyclerView = parentView.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(llm);
        progressBar = parentView.findViewById(R.id.progress);
    }
}
