package com.searchcity.mvp.model;

import com.searchcity.model.City;
import com.searchcity.model.CityItem;
import com.searchcity.retrofit.RetrofitApi;
import com.searchcity.utils.Constants;
import com.searchcity.utils.Utils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CityFragmentModel {

    public Observable<List<CityItem>> getCityByKeyObs(City city){
        return RetrofitApi.getService().getCity(city.getKey(), "ru", Constants.API_KEY)
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void deleteCityByKey(City city){
        Utils.deleteObject(City.class,"key", city.getKey());
    }
}
