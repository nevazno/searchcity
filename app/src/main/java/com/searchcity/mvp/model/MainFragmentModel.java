package com.searchcity.mvp.model;

import com.searchcity.model.City;
import com.searchcity.retrofit.RetrofitApi;
import com.searchcity.utils.Constants;
import com.searchcity.utils.Utils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;

public class MainFragmentModel {

    public void saveCities(List<City> cities){
        Utils.saveObjects(cities);
    }

    public Observable<List<City>> getCityListByNameObs(String value){
        return RetrofitApi.getService().getCityList(value, "ru", Constants.API_KEY)
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Flowable<RealmResults<City>> getRealmResult(Realm realm){
        return realm.where(City.class)
                .findAllAsync()
                .asFlowable()
                .observeOn(AndroidSchedulers.mainThread());
    }
}
