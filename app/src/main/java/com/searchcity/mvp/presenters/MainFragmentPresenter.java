package com.searchcity.mvp.presenters;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.searchcity.CityApp;
import com.searchcity.R;
import com.searchcity.model.City;
import com.searchcity.mvp.PositiveButtonCallback;
import com.searchcity.mvp.model.MainFragmentModel;
import com.searchcity.mvp.view.MainFragmentView;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.realm.RealmResults;

public class MainFragmentPresenter extends ParentPresenter{

    private MainFragmentModel model;
    private MainFragmentView view;

    public MainFragmentPresenter(MainFragmentModel model, MainFragmentView view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void onCreate(Activity activity, Bundle savedInstanceState) {
        super.onCreate(activity, savedInstanceState);
        PositiveButtonCallback callback = new PositiveButtonCallback() {
            @Override
            public void click(String value) {
                getCityListByName(value);
            }
        };
        view.setCallback(callback);
        subscribeToCitiesUpdates();
    }

    private void subscribeToCitiesUpdates(){
        Disposable disposable = model.getRealmResult(realm)
                .subscribe(new Consumer<RealmResults<City>>() {
                    @Override
                    public void accept(RealmResults<City> cities) {
                        Log.e("getCityList", "onNext " + cities.size());
                        view.showListCities(cities);
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void getCityListByName(String value) {
        view.showProgress();
        if(CityApp.getNetworkManager().isConnected()){
            model.getCityListByNameObs(value)
                    .subscribe(new Observer<List<City>>() {
                        @Override
                        public void onComplete() {
                            Log.e("getCityList", "onCompleted");
                            view.hideProgress();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("getCityList", "onError", e);
                            view.hideProgress();
                        }

                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(List<City> cities) {
                            Log.e("getCityList", "onNext " + cities.size());
                            String message = activity.getString(R.string.find) + cities.size() + activity.getString(R.string.city);
                            view.showToast(message);
                            model.saveCities(cities);
                        }
                    });
        } else {
            view.showToast(activity.getString(R.string.error_internet));
        }
    }

    public void clickMenuButtonAdd(){
        view.showAddCityDialog();
    }
}
