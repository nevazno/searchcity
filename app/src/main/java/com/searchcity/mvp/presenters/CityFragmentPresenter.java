package com.searchcity.mvp.presenters;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.searchcity.CityApp;
import com.searchcity.R;
import com.searchcity.model.City;
import com.searchcity.model.CityItem;
import com.searchcity.mvp.PositiveButtonCallback;
import com.searchcity.mvp.model.CityFragmentModel;
import com.searchcity.mvp.view.CityFragmentView;
import com.searchcity.utils.Utils;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CityFragmentPresenter extends ParentPresenter {

    private CityFragmentModel model;
    private CityFragmentView view;
    private City city;

    public CityFragmentPresenter(CityFragmentModel model, CityFragmentView view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void onCreate(Activity activity, Bundle savedInstanceState) {
        super.onCreate(activity, savedInstanceState);
        PositiveButtonCallback callback = new PositiveButtonCallback() {
            @Override
            public void click(String value) {
                model.deleteCityByKey(city);
            }
        };
        view.setCallback(callback);
        getCityByKey(city);
    }

    private void getCityByKey(City city){
        if(CityApp.getNetworkManager().isConnected()){
                model.getCityByKeyObs(city)
                    .subscribe(new Observer<List<CityItem>>() {
                        @Override
                        public void onComplete() {
                            Log.e("getCity", "onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("getCity", "onError", e);
                        }

                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(List<CityItem> cityItems) {
                            Log.e("getCity", "onNext " + cityItems.size());
                            view.showResponse(cityItems.get(0));
                        }
                    });
        } else {
            Utils.showToast(activity, activity.getString(R.string.error_internet));
        }
    }

    public void clickMenuButtonAdd(){
        view.showDeleteCityDialog();
    }

    public void setCity(City city) {
        this.city = city;
    }
}
