package com.searchcity.mvp.presenters;

import android.app.Activity;
import android.os.Bundle;

import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;

public class ParentPresenter implements Presenter {

    Activity activity;
    CompositeDisposable compositeDisposable;
    Realm realm;

    @Override
    public void onCreate(Activity activity, Bundle savedInstanceState) {
        compositeDisposable = new CompositeDisposable();
        this.activity = activity;
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onDestroy() {
        if(!compositeDisposable.isDisposed()){
            compositeDisposable.dispose();
        }
        if(!realm.isClosed()){
            realm.close();
        }
    }
}
