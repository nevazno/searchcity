package com.searchcity.mvp.presenters;

import android.app.Activity;
import android.os.Bundle;

public interface Presenter {
    void onCreate(Activity activity, Bundle savedInstanceState);
    void onDestroy();
}
