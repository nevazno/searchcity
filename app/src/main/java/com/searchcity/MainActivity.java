package com.searchcity;

import android.os.Bundle;

import com.searchcity.fragments.CityFragment;
import com.searchcity.fragments.MainFragment;
import com.searchcity.model.City;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolBar();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, MainFragment.newInstance())
                .commit();
    }

    public void showCityFragment(City city){
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, CityFragment.newInstance(city))
                .commit();
    }
}
