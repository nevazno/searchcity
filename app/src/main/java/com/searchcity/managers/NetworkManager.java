package com.searchcity.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkManager {
    private static final String TAG = NetworkManager.class.getSimpleName();
    private Context context;
    private NetworkInfo networkInfo;

    public NetworkManager(Context context) {
        this.context = context;
    }

    private void updateNetworkInfo() {
        ConnectivityManager cm = getConnectivityManager();
        networkInfo = cm.getActiveNetworkInfo();
    }

    private ConnectivityManager getConnectivityManager(){
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    private NetworkInfo getNetworkInfo() {
        updateNetworkInfo();
        displayLog();
        return networkInfo;
    }

    public boolean isConnected() {
        NetworkInfo currentNetworkInfo = getNetworkInfo();
        if(currentNetworkInfo != null){
            return currentNetworkInfo.isConnectedOrConnecting();
        } else {
            NetworkInfo mobNetInfo = getConnectivityManager().getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo wifiNetInfo = getConnectivityManager().getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            return mobNetInfo != null && mobNetInfo.isConnectedOrConnecting()
                    || wifiNetInfo != null && wifiNetInfo.isConnectedOrConnecting();
        }
    }

    private void displayLog() {
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            Log.d(TAG, "Network " + networkInfo.getTypeName() + " connected");
            //Toast.makeText(context,"Network " + networkInfo.getTypeName() + " connected",Toast.LENGTH_LONG).show();
        } else {
            Log.d(TAG, "There's no network connectivity");
            //Toast.makeText(context,"There's no network connectivity",Toast.LENGTH_LONG).show();
        }
    }
}