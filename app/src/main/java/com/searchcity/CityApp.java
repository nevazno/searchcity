package com.searchcity;

import android.app.Application;
import android.content.Context;

import com.searchcity.managers.NetworkManager;

import io.realm.Realm;

public class CityApp extends Application {

    private static CityApp instance;
    private NetworkManager networkManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        networkManager = new NetworkManager(getApplicationContext());
        instance = this;
    }

    public static Context getAppContext() {
        return instance.getApplicationContext();
    }

    public static NetworkManager getNetworkManager() {
        return instance.networkManager;
    }
}
