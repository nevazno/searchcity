package com.searchcity.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import io.realm.RealmObject;

public abstract class RecycleAdapter<OB extends RealmObject, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private List<OB> items;
    protected abstract VH setViewHolder(ViewGroup parent);
    protected abstract void onBindData(VH holder, OB val, int position);
    protected RecycleAdapter(List<OB> items) {
        this.items = items;
    }
    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return setViewHolder(parent);
    }
    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        onBindData(holder, getItem(position), position);
    }
    @Override
    public int getItemCount() {
        return items.size();
    }
    private OB getItem(int position) {
        return items.get(position);
    }
}