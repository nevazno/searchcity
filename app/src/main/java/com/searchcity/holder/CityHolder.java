package com.searchcity.holder;


import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.searchcity.R;

public class CityHolder extends RecyclerView.ViewHolder{
    public TextView name;
    public TextView area;
    public TextView country;

    public CityHolder(View v) {
        super(v);
        name = v.findViewById(R.id.name);
        area = v.findViewById(R.id.area);
        country = v.findViewById(R.id.country);
    }
}
