package com.searchcity.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.searchcity.R;
import com.searchcity.model.City;
import com.searchcity.mvp.holder.CityFragmentHolder;
import com.searchcity.mvp.model.CityFragmentModel;
import com.searchcity.mvp.presenters.CityFragmentPresenter;
import com.searchcity.mvp.view.CityFragmentView;

public class CityFragment extends Fragment {

    private City city;
    private CityFragmentPresenter presenter;

    public static CityFragment newInstance(City city) {
        CityFragment fragment = new CityFragment();
        fragment.city = city;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_city, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View parentView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(parentView, savedInstanceState);
        CityFragmentHolder holder = new CityFragmentHolder(parentView);
        CityFragmentModel model = new CityFragmentModel();
        CityFragmentView view = new CityFragmentView(holder, getActivity());
        presenter = new CityFragmentPresenter(model, view);
        presenter.setCity(city);
        presenter.onCreate(getActivity(), savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_currentconditions, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_remove) {
            presenter.clickMenuButtonAdd();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(presenter != null){
            presenter.onDestroy();
        }
    }
}
