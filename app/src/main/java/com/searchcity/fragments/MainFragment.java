package com.searchcity.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.searchcity.R;
import com.searchcity.mvp.holder.MainFragmentHolder;
import com.searchcity.mvp.model.MainFragmentModel;
import com.searchcity.mvp.presenters.MainFragmentPresenter;
import com.searchcity.mvp.view.MainFragmentView;

public class MainFragment extends Fragment {

    private MainFragmentPresenter presenter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View parentView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(parentView, savedInstanceState);
        MainFragmentHolder holder = new MainFragmentHolder(parentView, getContext());
        MainFragmentModel model = new MainFragmentModel();
        MainFragmentView v = new MainFragmentView(holder, getActivity());
        presenter = new MainFragmentPresenter(model, v);
        presenter.onCreate(getActivity(), savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            presenter.clickMenuButtonAdd();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(presenter != null){
            presenter.onDestroy();
        }
    }
}
