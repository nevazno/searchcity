package com.searchcity.retrofit;

import com.searchcity.model.City;
import com.searchcity.model.CityItem;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import io.reactivex.Observable;

public interface RetrofitService {

    @GET("/locations/v1/cities/search.json")
    Observable<List<City>> getCityList(@Query("q") String q, @Query("language") String language, @Query("apikey") String apiKey);

    @GET("/currentconditions/v1/{city_id}.json/")
    Observable<List<CityItem>> getCity(@Path("city_id") int cityId, @Query("language") String language, @Query("apikey") String apiKey);
}
