package com.searchcity.utils;


import android.content.Context;
import android.widget.Toast;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class Utils {

    public static void saveObjects(List<? extends RealmObject> list){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(list);
        realm.commitTransaction();
        realm.close();
    }

    public static void deleteObject(Class<? extends RealmObject> clazz, String field, int value){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<? extends RealmObject> results = realm.where(clazz).equalTo(field,value).findAll();
        if (!results.isEmpty()) {
            results.deleteAllFromRealm();
        }
        realm.commitTransaction();
        realm.close();
    }

    public static void showToast(final Context context, final String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
